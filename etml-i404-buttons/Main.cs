﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace etml_i404_buttons
{
    public partial class Main : Form
    {
        /// <summary>
        /// Nom de l'utilisateur.
        /// </summary>
        private string name;

        public Main()
        {
            InitializeComponent();

            // Boutons initialement désactivés
            btnStart.Enabled = false;
            btnEnd.Enabled = false;

            // Nom initialement vide
            name = "";
        }

        /// <summary>
        /// Affiche le message de bienvenue "salut" dans le label.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStart_Click(object sender, EventArgs e)
        {
            // Afficher le message dans le label
            lblHi.Text = "quoi " + name;
        }

        /// <summary>
        /// Affiche un message d'adieu dans le label.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEnd_Click(object sender, EventArgs e)
        {
            // Afficher le message d'adieu
            lblHi.Text = "feur " + name;
        }

        /// <summary>
        /// Fermer le programme.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStop_Click(object sender, EventArgs e)
        {
            // Quitter le programme
            Application.Exit();
        }

        /// <summary>
        /// Boite de texte permettant d'entrer le nom de l'utilisateur.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtName_TextChanged(object sender, EventArgs e)
        {
            // Nom d'utilisateur
            name = txtName.Text;

            // Activier les boutons si un nom a été entré
            if (txtName.Text.Length == 0)
            {
                btnStart.Enabled = btnEnd.Enabled = false;
            }
            else
            {
                btnStart.Enabled = btnEnd.Enabled = true;
            }
        }
    }
}
